#!/bin/bash
#
# bashcat -
# Its like hashcat, but in bash...
# by radix
#

IFS=' 	
'

if [ $# -eq "0" ]; then

cat<<EOF
bashcat, non-advanced password recovery
 
Usage: bashcat.sh [options] hashfile wordlist
 
=======
Options
=======
 
* General:
 
-m              Hash-type, see references below
 
* Hash types:
 
     0 = MD5
    10 = md5(\$pass.\$salt)
    20 = md5(\$salt.\$pass)
    30 = md5(unicode(\$pass).\$salt)
   100 = SHA1
   900 = MD4
  1000 = NTLM
  1400 = SHA256
  1700 = SHA512
EOF

exit 0

fi

while getopts m:h:o opt; do
  case $opt in
    m)
	   if [ $OPTARG == 0 ]; then
	   	algo=md5sum
	   	mode=0
	   	shift;
	   fi

	   if [ $OPTARG == 10 ]; then
	   	algo=md5sum
	   	mode=10
	   	salted=1
	   	shift;
	   fi

	   if [ $OPTARG == 20 ]; then
	   	algo=md5sum
	   	mode=20
	   	salted=1
	   	shift;
	   fi

	   if [ $OPTARG == 30 ]; then
	   	algo=md5sum
	   	mode=30
	   	salted=1
	   	shift;
	   fi

	   if [ $OPTARG == 100 ]; then
	   	algo=sha1sum
	   	mode=100
	   	shift;
	   fi

	   if [ $OPTARG == 900 ]; then
	   	algo="openssl dgst -md4"
	   	mode=900
	   	shift;
	   fi

	   if [ $OPTARG == 1000 ]; then
	   	algo="openssl dgst -md4"
	   	mode=1000
	   	shift;
	   fi

	   if [ $OPTARG == 1400 ]; then
	   	algo=sha256sum
	   	mode=1400
	   	shift;
	   fi

	   if [ $OPTARG == 1700 ]; then
	   	algo=sha512sum
	   	mode=1700
	   	shift;
	   fi
	   ;;
  esac
done

hash_list=$2
word_list=$3

word_count=`wc -l $3 | cut -d ' ' -f 1`
hash_count=`wc -l $2 | cut -d ' ' -f 1`
crack_count=0
timestamp="$(date +%s)"

function new_hash
{
	new_hash=`echo ${hash} | cut -d ':' -f 1`
}

function salt
{
	salt=`echo ${hash} | cut -d ':' -f 2`
}

if [ $salted == "1" ]; then

	salt_count=`cut -d ':' -f 2 ${hash_list} | sort -u | wc -l`

fi

echo "Initializing bashcat v0.01 by radix..."
echo ""
echo "Added hashes from file $hash_list: $hash_count ($salt_count salts)"
echo ""
if [ $hash_count == "1" ]; then
	echo "If this were more advanced, we would activate quick-digest mode for your single hash"
	echo ""
fi
echo "Hash.List.: ${hash_list}"
echo "Mode......: ${mode}"
echo "Input.Mode: Dict (${word_list})"
echo "Recovered.: 0/${hash_count} hashes, 0/${salt_count} salts"
echo ""
echo "NOTE: you can press enter, but it won't do anything"
echo ""

for hash in `cat ${hash_list}`; do

  for word in `cat ${word_list}`; do

  	if [ ${mode} == 10 ]; then

  		new_hash
  		
  		salt

  		result=`echo -n "${word}${salt}" | ${algo} | egrep -io '[A-Fa-f0-9]{32}'`

  	elif [ ${mode} == 20 ]; then

  		new_hash
  		
  		salt

  		result=`echo -n "${salt}${word}" | ${algo} | egrep -io '[A-Fa-f0-9]{32}'`

  	elif [ ${mode} == 30 ]; then

  		new_hash
  		
  		salt

  		convert=`printf "${word}" | iconv -t utf16le`

  		printf ${convert}

  		result=`printf "${convert}${salt}" | ${algo} | egrep -io '[A-Fa-f0-9]{32}'`

  	elif [ ${mode} == 100 ]; then

    	result=`echo -n "${word}" | ${algo} | egrep -io '[A-Fa-f0-9]{40}'`
  	
  	elif [ ${mode} == 1000 ]; then

  		result=`echo -n "${word}" | iconv -t utf16le | ${algo} | egrep -io '[A-Fa-f0-9]{32}'`
    
    elif [ ${mode} == 1400 ]; then

    	result=`echo -n "${word}" | ${algo} | egrep -io '[A-Fa-f0-9]{64}'`

    elif [ ${mode} == 1700 ]; then

    	result=`echo -n "${word}" | ${algo} | egrep -io '[A-Fa-f0-9]{128}'`

    else

    	result=`echo -n "${word}" | ${algo} | egrep -io '[A-Fa-f0-9]{32}'`

    fi

    if [ -z ${salt} ]; then

	    if [ ${result} == ${hash} ]; then

	    	echo ${hash}:${word}

      	fi

    else

    	if [ ${result} == ${new_hash} ]; then

    		echo ${new_hash}:${salt}:${word}

    	fi

      crack_count=$((crack_count+1))

      break

    fi

  done

done

timestamp="$(($(date +%s)-timestamp))"

echo ""
echo "Hash.List.: ${hash_list}"
echo "Mode......: ${mode}"
echo "Input.Mode: Dict (${word_list})"
echo "Recovered.: ${crack_count}/${hash_count} hashes, 0/${salt_count} salts"
printf "Speed/sec.: %02d plains\n" "$(($word_count/timestamp))"
echo "Progress..: ${word_count}/${word_count} (100%)"
printf "Runtime...: %02d:%02d:%02d:%02d\n" "$((timestamp/86400))" "$((timestamp/3600%24))" "$((timestamp/60%60))" "$((timestamp%60))"
